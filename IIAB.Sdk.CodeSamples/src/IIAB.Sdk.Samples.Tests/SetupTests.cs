using System.IO;
using NUnit.Framework;

namespace IIAB.Sdk.Samples.Tests
{
  public class Tests
  {
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void install_codesamples_should_install_samples()
    {
      var path = @"d:\temp\samples";
      var di = new DirectoryInfo(path);
      var result  = Samples.Setup.Setup.Install(di);
      Assert.IsTrue(result);
    }
  }
}