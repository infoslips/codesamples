<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\Accessibility.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Configuration.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Deployment.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Formatters.Soap.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Security.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Windows.Forms.dll</Reference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <Namespace>RestSharp</Namespace>
</Query>

private string _baseUri = "";
private string _userName = "";
private string _password = "";

private string _jsonDataFile = @"";
private string _runTemplateId = "";

RestClient _restClient;

void Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));

	_login(_baseUri, _userName, _password);

	var json = File.ReadAllText(_jsonDataFile);

	var adhocModel = _getAddHocModel(json, _runTemplateId);

	adhocModel.Dump("Returned Adhoc model");

	//MessageBox.Show("Paste in browser: " + adhocModel.ViewerUrl);
}

private AdhocViewResponseModel _getAddHocModel(string payload, string runTemplateId)
{
	var adhocData = new { RunTemplateId = runTemplateId, ProcessedData = payload };
	var adHocRequest = new RestRequest("api/adhoc/send", Method.POST) { RequestFormat = DataFormat.Json };
	adHocRequest.AddJsonBody(adhocData);

	var adHocResponse = _restClient.Execute<AdhocViewResponseModel>(adHocRequest);
	if (adHocResponse.StatusCode != System.Net.HttpStatusCode.OK)
		throw new InvalidOperationException(string.Format("Failed to post the adhoc data: {0}\n{1}", adHocResponse.StatusDescription, adHocResponse.Content));

	return adHocResponse.Data;
}


private void _login(string baseUri, string username, string password)
{
	_restClient = new RestClient(baseUri);

	if (_restClient == null)
		throw new InvalidOperationException("Failed to open a connection the api");

	var loggedInResponse = _loginRestResponse(_restClient, username, password);
	_restClient.AddDefaultHeader("Authorization", string.Format("{0} {1}", loggedInResponse.Data.token_type, loggedInResponse.Data.access_token));
}

private IRestResponse<LoggedInTokenModel> _loginRestResponse(IRestClient client, string username, string password)
{
	const string baseUri = "oauth";
	var request = new RestRequest(baseUri, Method.POST);
	request.AddParameter("username", username);
	request.AddParameter("password", password);
	request.AddParameter("grant_type", "password");
	request.AddParameter("client_id", "ifscloudapp");
	// action
	var restResponse = client.Execute<LoggedInTokenModel>(request);
	return restResponse;
}

public class LoggedInTokenModel
{
	public string access_token { get; set; }
	public string token_type { get; set; }
	public int expires_in { get; set; }
	public string userName { get; set; }
	public DateTime Issued { get; set; }
	public DateTime Expires { get; set; }

}

public class AdhocViewResponseModel
{
	public bool IsSuccess { get; set; }
	public string AdhocId { get; set; }
	public string ViewerUrl { get; set; }
}