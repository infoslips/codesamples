<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\Accessibility.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Configuration.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Deployment.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Formatters.Soap.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Security.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Windows.Forms.dll</Reference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <Namespace>RestSharp</Namespace>
  <Namespace>System.Windows.Forms</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
</Query>

private string _baseUri = "";
private string _userName = "";
private string _password = "";

private string _jsonDataFile = @"";
private string _runTemplateId = "";

private string _toMobile = "";

RestClient _restClient;

void Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));

	if (string.IsNullOrEmpty(_toMobile))
		_toMobile = Util.ReadLine("Please enter a mobile number");

	_login(_baseUri, _userName, _password);

	var json = File.ReadAllText(_jsonDataFile);

	_forwardAdhocSms(json, _runTemplateId, _toMobile);
}

void _forwardAdhocSms(string payload, string runTemplateId, string to)
{
	if (!_validateAddress(to))
		return;

	var adhocForward = new
	{
		MobileNumber = to,
		ExpiresInSeconds = 1800, // Default 30min
		RunTemplateId = runTemplateId,
		ProcessedData = payload
		// Replace ProcessedData with CompressedProcessedData as below. Recomended method for quicker response times
		// CompressedProcessedData = CompressedPayload()
	};

	var request = new RestRequest("api/adhoc/forwardsms", Method.POST);
	request.AddJsonBody(adhocForward);
	_validateAndPrintResponse(request);
}

private bool _validateAddress(string to)
{
	if (string.IsNullOrEmpty(to))
	{
		"Skip sending because to address is empty".Dump();
		return false;
	}
	return true;
}
void _validateAndPrintResponse(RestRequest request)
{
	var response = _restClient.Execute(request);

	if (response.StatusCode != HttpStatusCode.OK)
	{
		$"{response.StatusCode} : {response.StatusDescription} - {response.Content}".Dump("I'm sorry");
		return;
	}
	response.Content.Dump("Done");
}
private void _login(string baseUri, string username, string password)
{
	_restClient = new RestClient(baseUri);

	if (_restClient == null)
		throw new InvalidOperationException("Failed to open a connection the api");

	var loggedInResponse = _loginRestResponse(_restClient, username, password);
	_restClient.AddDefaultHeader("Authorization", string.Format("{0} {1}", loggedInResponse.Data.token_type, loggedInResponse.Data.access_token));
}
private IRestResponse<LoggedInTokenModel> _loginRestResponse(IRestClient client, string username, string password)
{
	const string baseUri = "oauth";
	var request = new RestRequest(baseUri, Method.POST);
	request.AddParameter("username", username);
	request.AddParameter("password", password);
	request.AddParameter("grant_type", "password");
	request.AddParameter("client_id", "ifscloudapp");
	// action
	var restResponse = client.Execute<LoggedInTokenModel>(request);
	return restResponse;
}

public class LoggedInTokenModel
{
	public string access_token { get; set; }
	public string token_type { get; set; }
	public int expires_in { get; set; }
	public string userName { get; set; }
	public DateTime Issued { get; set; }
	public DateTime Expires { get; set; }

}