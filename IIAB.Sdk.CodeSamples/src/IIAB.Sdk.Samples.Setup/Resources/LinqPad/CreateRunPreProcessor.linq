<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Collections.Concurrent.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Threading.dll</Reference>
  <NuGetReference>FluentAssertions</NuGetReference>
  <NuGetReference>log4net</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>SharpZipLib</NuGetReference>
  <Namespace>ICSharpCode.SharpZipLib.GZip</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>RestSharp</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

string _baseUri = "";
DirectoryInfo _sourceDataDirInfo = new DirectoryInfo("");
string _runTemplateId = "";
string _runDescription = $"Test API Pre-Processor Integration - {DateTime.Now.ToString("dd-MM-yyyy")}";
bool _runIsTrial = true;

List<string> _trialEmailAddresses = new List<string> { "verifiedemail@yourdomain.com" };

string _userName = "";
string _password = "";

IRestClient _restClient;

async Task Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));
		
	_login(_baseUri, _userName, _password);

	var runProfileId = await _createRun(_restClient, _runDescription, _runIsTrial, _runTemplateId, _trialEmailAddresses);

	// TODO: [OPT] IFS - Configuration Files Upload
	
	// TODO: [OPT] IFS - Static Attachment Files Upload
	
	// TODO: [OPT] IFS - Static Run Recipient Attachment Files Upload

	// Loop through all configured files required by your configured pre-processor
	// This information will be supplied by InfoSlips during project takeon
	foreach (var file in _sourceDataDirInfo.GetFiles("*.csv"))
	{
		await _uploadRunFile(_restClient, runProfileId, file.FullName);
	}

	await _startRun(_restClient, runProfileId, _runIsTrial);
}

private async Task _uploadRunFile(IRestClient client, string runId, string filename)
{
	var file = new FileInfo(filename);
	if (!file.Exists)
		throw new InvalidOperationException($"Failed to find file {file}");
	
	// TODO: [OPT] IFS - Data can be compressed and uploaded as gzip stream.
	// This is prefered for larger data sets in order to increase load and performance
	var fileStream = File.ReadAllBytes(file.FullName);
	
	var request = new RestRequest($"api/run/{runId}/UploadFiles/{file.Name}", Method.POST) { RequestFormat = DataFormat.Json };
	request.AddFile(file.Name,fileStream, filename,"application/octet-stream");
	var response = await client.ExecutePostAsync(request);
	if (response.StatusCode != HttpStatusCode.OK)
	{
		throw new InvalidOperationException($"Failed to upload file {file}");
	}
}

private async Task<string> _createRun(IRestClient client, string runDescription,bool isTrial,string runTemplateId,List<string> trailEmailAddresses)
{
	var json = _getRunConfig(runDescription,isTrial,runTemplateId,trailEmailAddresses);
	var request = new RestRequest("api/run/createRun", Method.POST) { RequestFormat = DataFormat.Json };
	var data = string.Format("{0}", JsonConvert.SerializeObject(json));
	request.AddBody(json);
	var response = await client.ExecutePostAsync(request);
	if (response.StatusCode == HttpStatusCode.OK)
	{
		var run = _convertJson(response.Content);
		return run.Id;
	}
	throw new InvalidOperationException(string.Format("Failed to create the run: {0}-{1},\n{2}", response.StatusCode, response.StatusDescription, response.ErrorMessage));
}

private async Task _startRun(IRestClient client, string runId, bool isTrial)
{
	var request = new RestRequest($"api/run/{runId}/startRun/{isTrial}/0", Method.POST);
	var response = await client.ExecutePostAsync(request);

	if (response.StatusCode == HttpStatusCode.OK)
	{
		var run = _convertJson(response.Content);
		return;
	}
	throw new InvalidOperationException(string.Format("Failed to create the run: {0}-{1},\n{2}", response.StatusCode, response.StatusDescription, response.ErrorMessage));
}

private dynamic _getRunConfig(string runDescription, bool isTrial, string runTemplateId,List<string> trialEmailAddresses)
{
	return new
	{
	 	// Name of the run that will be displayd to users
		Name = runDescription,
		
		// Will samples be reviewed before release 	  
		IsTrial = isTrial, 
		
		// Default Value
		IsPukLess = true, 
		
		// For future dated runs. This does require the run to be in non-trial state
		//ScheduleDateTime = DateTime.Now.AddMonths(1),
		
		// Template id to be used when creating the run
		RunTemplateId = runTemplateId,
		
		// Should Trial be true, then what should setup be
		TrialOptions = new
		{
			// Should a trial be sent immediatly when the all data is uploaded
			IsAutoTrialEnabled = false,
			
			// Default number of recipient selected for trial -- Try never to exceed 10, it will overwelm users
			/*
				OneRandomInfoslip,FiveRandomInfoslip,TenRandomInfoslip,ThirtyRandomInfoSlips,SpecificInfoslip,AllInfoslip
			*/
			RecipientSelection = 1,
			
			/*
			 UserKey,SampleKey,NoKey
			*/
			UserKeyOption = 2,
			
			/*
			All,Ifs,Pdf,Sms
			*/
			FilterOutputType = 0,
			
			SendToUser = trialEmailAddresses,
			
			// Leave as uninitialized list, will be loaded by {RecipientSelection} specified
			RecipientList = new List<string>()
		}
	};
}

private dynamic _convertJson(string json)
{
	return JsonConvert.DeserializeObject<dynamic>(json);
}

private void _login(string baseUri, string username, string password)
{
	_restClient = new RestClient(baseUri);

	if (_restClient == null)
		throw new InvalidOperationException("Failed to open a connection the api");

	var loggedInResponse = _loginRestResponse(_restClient, username, password);
	_restClient.AddDefaultHeader("Authorization", string.Format("{0} {1}", loggedInResponse.Data.token_type, loggedInResponse.Data.access_token));
}

private IRestResponse<LoggedInTokenModel> _loginRestResponse(IRestClient client, string username, string password)
{
  const string baseUri = "oauth";
  var request = new RestRequest(baseUri, Method.POST);
  request.AddParameter("username", username);
  request.AddParameter("password", password);
  request.AddParameter("grant_type", "password");
  request.AddParameter("client_id", "ifscloudapp");
  // action
  var restResponse = client.Execute<LoggedInTokenModel>(request);
  return restResponse;
}


public class LoggedInTokenModel
{
public string access_token { get; set; }
public string token_type { get; set; }
public int expires_in { get; set; }
public string userName { get; set; }
public DateTime Issued { get; set; }
public DateTime Expires { get; set; }

}