<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\Accessibility.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Configuration.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Deployment.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Formatters.Soap.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Security.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Windows.Forms.dll</Reference>
  <NuGetReference>IIAB.Sdk.Samples</NuGetReference>
  <Namespace>GraphQL</Namespace>
  <Namespace>GraphQL.Client.Http</Namespace>
  <Namespace>GraphQL.Client.Serializer.Newtonsoft</Namespace>
  <Namespace>IIAB.Core.Common.Helpers</Namespace>
  <Namespace>IIAB.Core.Common.Models</Namespace>
  <Namespace>IIAB.Sdk</Namespace>
  <Namespace>IIAB.Web.Models</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

private string _baseUri = "";
private string _userName = "";
private string _password = "";

private InfoSlipsApiSession _session;
private GraphQLHttpClient _client;

async Task Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));

	_session = new InfoSlipsApiSession(new System.Net.Http.HttpClient { BaseAddress = new Uri(_baseUri) });
	await _session.Authorize.Login(_userName, _password);
	
	var options = new GraphQLHttpClientOptions()
	{
		EndPoint = new Uri(_baseUri+"graphql")
	};
	_client = new GraphQLHttpClient(options, new NewtonsoftJsonSerializer(), _session.Client);
	
	var request = new GraphQLRequest()
      {
        Query = @"query {
						  historyCards{
						    nodes {
						      period,
						      runDate,
						      runName,
						      documentType,
						      id
						    }
						  }
						}"
	};

	var response = await _client.SendQueryAsync<dynamic>(request);
	var jsonDataResponse = JsonConvert.SerializeObject(response.Data);
	var responseData = (ResponseData)JsonConvert.DeserializeObject<ResponseData>(jsonDataResponse);
	
	//Run Recipient History
	var list = responseData.Dump();
}


public class ResponseData
{
  public Historycards historyCards { get; set; }
}

public class Historycards
{
  public Node[] nodes { get; set; }
}

public class Node
{
  public string period { get; set; }
  public DateTime runDate { get; set; }
  public string runName { get; set; }
  public string documentType { get; set; }
  public string id { get; set; }
}




