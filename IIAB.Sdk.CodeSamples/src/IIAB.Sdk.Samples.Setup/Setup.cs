﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace IIAB.Sdk.Samples.Setup
{
  public static class Setup
  {
    public static bool Install(DirectoryInfo installPath)
    {
      return ExtractResources(installPath);
    }

    internal static string GetCodeSampleText()
    {
      return ExtractCodeSampleText().Substring(1);
    }

    private static byte[] ToBytes(this Stream stream)
    {
      var mem = new MemoryStream();
      stream.CopyTo(mem);
      mem.Position = 0;
      return mem.ToArray();
    }

    private static string ExtractCodeSampleText()
    {
      var assembly = typeof(Setup).GetTypeInfo().Assembly;
      var resources = assembly.GetManifestResourceNames();
      var resource = resources.FirstOrDefault(x => x == "IIAB.Sdk.Samples.Setup.Resources.CodeSample.txt");
      var stream = assembly.GetManifestResourceStream(resource);
      if (stream != null)
        return Encoding.UTF8.GetString(stream.ToBytes());
      return string.Empty;
    }

    internal static void Write(ConsoleColor color, string text)
    {
      var initColor = Console.ForegroundColor;
      Console.ForegroundColor = color;
      Console.Write(text);
      Console.ForegroundColor = initColor;
    }

    internal static void Write(ConsoleColor color, StringBuilder text)
    {
      var initColor = Console.ForegroundColor;
      Console.ForegroundColor = color;
      Console.Write(text.ToString());
      Console.ForegroundColor = initColor;
    }

    internal static void WriteLine(ConsoleColor color, string text)
    {
      var initColor = Console.ForegroundColor;
      Console.ForegroundColor = color;
      Console.WriteLine(text);
      Console.ForegroundColor = initColor;
    }

    private static bool ExtractResources(DirectoryInfo installPath)
    {
      if (!installPath.Exists)
        installPath.Create();

      var result = new Dictionary<string, int>();

      var folders = new List<string>
      {
        "Lib",
        "LinqPad",
        "Postman",
        "GraphQl"
      };

      var assembly = typeof(Setup).GetTypeInfo().Assembly;
      var resources = assembly.GetManifestResourceNames();
      var failCnt = 0;
      foreach (var folder in folders)
      {
        var folderPath = $"IIAB.Sdk.Samples.Setup.Resources.{folder}";

        var files = resources.Where(x => x.StartsWith(folderPath)).ToList();
        if (!files.Any()) continue;
        var cnt = 0;

        Write(ConsoleColor.Cyan, "Extracting files to folder:");
        WriteLine(ConsoleColor.Blue, $"[{folder}]");
        foreach (var file in files)
        {
          var filename = file.Replace($"{folderPath}.", "");

          Write(ConsoleColor.DarkGray, "Extracting file");
          Write(ConsoleColor.DarkCyan, $" {filename}...");
          try
          {
            using (var progress = new ProgressBar())
            {
              if (result.ContainsKey(filename)) continue;
              var baseDir = new DirectoryInfo(Path.Combine(installPath.FullName, folder));
              if (!baseDir.Exists)
                baseDir.Create();

              using (var resource = Assembly.GetExecutingAssembly().GetManifestResourceStream(file))
              {
                var outFile = new FileInfo(Path.Combine(baseDir.FullName, filename));
                using (var stream = new FileStream(outFile.FullName, FileMode.Create, FileAccess.Write))
                {
                  resource?.CopyTo(stream);
                  result.Add($"{folder}{filename}", 1);
                }
                progress.Report((double)++cnt / files.Count);
                Thread.Sleep(400);
              }
            }
            WriteLine(ConsoleColor.Green, "[√ done]");
          }
          catch (Exception)
          {
            ++failCnt;
            WriteLine(ConsoleColor.Red, "[x fail]");
          }
        }
      }
      if (failCnt > 0)
      {
        WriteLine(ConsoleColor.Blue, "╔═════════════════════════════════════════════════╗");
        Write(ConsoleColor.Blue, "║");
        Write(ConsoleColor.DarkRed, "Some Errors occurred, please ensure you have      ");
        WriteLine(ConsoleColor.Blue, "║");
        Write(ConsoleColor.Blue, "║");
        Write(ConsoleColor.DarkRed, "access to the installation folder, and try again!");
        WriteLine(ConsoleColor.Blue, "║");
        WriteLine(ConsoleColor.Blue, "╚═════════════════════════════════════════════════╝");
      }
      else
      {
        WriteLine(ConsoleColor.Blue, "╔═════════════════════════════════════════════════╗");
        Write(ConsoleColor.Blue, "║");
        Write(ConsoleColor.Green, "Successfully installed Sample Code...            ");
        WriteLine(ConsoleColor.Blue, "║");
        WriteLine(ConsoleColor.Blue, "╚═════════════════════════════════════════════════╝");
      }
      return true;
    }
  }
}