﻿using System;
using System.IO;

namespace IIAB.Sdk.Samples.Setup
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.Clear();
      Setup.WriteLine(ConsoleColor.Green,Setup.GetCodeSampleText());
      
      var path = string.Empty;
      if (args.Length < 1)
      {
        Setup.WriteLine(ConsoleColor.Red,"Usage: IIAB.Sdk.Samples.Setup [Location]");
        Setup.Write(ConsoleColor.DarkGreen,"Please enter folder to install samples to:");
        path = Console.ReadLine();
      }
      else
      {
        path = args[0];
      }

      try
      {
        //validate that is valid path
        path = Path.GetFullPath(path);

        var di = new DirectoryInfo(path);
        Setup.Write(ConsoleColor.DarkMagenta, "Installing Sample Code to:");
        Setup.WriteLine(ConsoleColor.Cyan,$" {di.FullName}...");
        Setup.WriteLine(ConsoleColor.Magenta, "Press Any Key to continue...");
        Console.ReadKey();
        if (!di.Exists)
          di.Create();

        Setup.Install(di);

        Setup.Write(ConsoleColor.Yellow,"Press Any Key to continue...");
        Console.ReadKey();
        Console.Clear();
      }
      catch (Exception)
      {
        Setup.WriteLine(ConsoleColor.Red, "Usage: IIAB.Sdk.Samples.Setup [Location]");
        Setup.Write(ConsoleColor.Yellow, "Press Any Key to continue...");
        Console.ReadKey();
        Console.Clear();
      }
    }
  }
}
