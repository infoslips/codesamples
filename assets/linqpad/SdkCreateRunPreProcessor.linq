<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Collections.Concurrent.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Threading.dll</Reference>
  <NuGetReference>IIAB.Sdk.Samples</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>IIAB.Sdk</Namespace>
  <Namespace>IIAB.Web.Models</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

private string _baseUri = "";
private string _userName = "";
private string _password = "";

private DirectoryInfo _sourceDataDirInfo = new DirectoryInfo(@"");
private string _runTemplateId = "";
private string _runDescription = $"Test API Pre-Processor Integration - {DateTime.Now.ToString("dd-MM-yyyy")}";
private bool _runIsTrial = true;

private List<string> _trialEmailAddresses = new List<string> { "verifiedemail@yourdomain.com" };

private InfoSlipsApiSession _session;

async Task Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));

	_session = new InfoSlipsApiSession(new System.Net.Http.HttpClient { BaseAddress = new Uri(_baseUri) });
	await _session.Authorize.Login(_userName, _password);

	var run = await _session.Runs.Create(new CreateRunModel{
		// Name of the run that will be displayd to users
		Name = _runDescription,

		// Will samples be reviewed before release 	  
		IsTrial = _runIsTrial,

		// Default Value
		IsPukLess = true,

		// For future dated runs. This does require the run to be in non-trial state
		//ScheduleDateTime = DateTime.Now.AddMonths(1),

		// Template id to be used when creating the run
		RunTemplateId = _runTemplateId,

		// Should Trial be true, then what should setup be
		TrialOptions = new TrialOptionsModel
		{
			// Should a trial be sent immediatly when the all data is uploaded
			IsAutoTrialEnabled = false,

			// Default number of recipient selected for trial -- Try never to exceed 10, it will overwelm users
			/*
				OneRandomInfoslip,FiveRandomInfoslip,TenRandomInfoslip,ThirtyRandomInfoSlips,SpecificInfoslip,AllInfoslip
			*/
			RecipientSelection = IIAB.Core.Common.Enum.RecipientSelectionOptions.OneRandomInfoslip,

			/*
			 UserKey,SampleKey,NoKey
			*/
			UserKeyOption = IIAB.Core.Common.Enum.UserKeyOptions.NoKey,

			/*
			All,Ifs,Pdf,Sms
			*/
			FilterOutputType = IIAB.Core.Common.Enum.FilterOutputTypeCodes.All,

			SendToUser = _trialEmailAddresses,

			// Leave as uninitialized list, will be loaded by {RecipientSelection} specified
			RecipientList = new List<string>()
		}
	});

	var fi = _sourceDataDirInfo.GetFiles("*.*");
	
	// Configuration Files Upload
	await _session.Runs.AddFileForProcessing(run.Id,"ConfigForProcessing",fi.FirstOrDefault(x=> x.Name == "ConfigForProcessing"));
	
	// TODO: [OPT] IFS - Static Attachment Files Upload
	
	// TODO: [OPT] IFS - Static Run Recipient Attachment Files Upload
	
	// Loop through all configured files required by your configured pre-processor
	// This information will be supplied by InfoSlips during project takeon
	foreach (var file in fi.Where(x => x.Extension.Contains(".xlsx")))
	{
		await _session.Runs.AddFileForProcessing(run.Id,file.Name,file);
	}

	await _session.Runs.AddToQueue(run.Id);
}