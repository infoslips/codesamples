<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\Accessibility.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Configuration.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Deployment.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Formatters.Soap.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Security.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Windows.Forms.dll</Reference>
  <NuGetReference>IIAB.Sdk.Samples</NuGetReference>
  <Namespace>IIAB.Sdk</Namespace>
  <Namespace>IIAB.Web.Models</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

private string _baseUri = "";
private string _userName = "";
private string _password = "";

private string _jsonDataFile = @"";
private string _runTemplateId = "";

private InfoSlipsApiSession _session;

async Task Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));

	_session = new InfoSlipsApiSession(new System.Net.Http.HttpClient { BaseAddress = new Uri(_baseUri) });
	await _session.Authorize.Login(_userName, _password);

	var payload = IIAB.Core.Common.Helpers.CompressionHelper.GZipCompress(File.ReadAllBytes(_jsonDataFile));

	var adhocModel = await _session.AdhocViews.Forward(new AdhocForwardModel{
		RunTemplateId = _runTemplateId,
		CompressedProcessedData = payload
	});

	adhocModel.Dump("Returned Adhoc model");
}
