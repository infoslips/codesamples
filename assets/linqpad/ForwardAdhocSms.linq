<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\Accessibility.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Configuration.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Deployment.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Formatters.Soap.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Security.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Windows.Forms.dll</Reference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>SharpZipLib</NuGetReference>
  <Namespace>ICSharpCode.SharpZipLib.GZip</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>RestSharp</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>System.Windows.Forms</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

private string _baseUri = "";
private string _userName = "";
private string _password = "";

private string _jsonDataFile = @"";
private string _runTemplateId = "";

private string _toMobile = "";

RestClient _restClient;

async Task Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));

	if (string.IsNullOrEmpty(_toMobile))
		_toMobile = Util.ReadLine("Please enter a mobile number");

	_login(_baseUri, _userName, _password);

	var json = File.ReadAllBytes(_jsonDataFile);

	var adhocModel = await _forwardAdhocSms(json, _runTemplateId, _toMobile);
	
	adhocModel.Dump("Returned Adhoc model");
}

private async Task<AdhocViewResponseModel> _forwardAdhocSms(byte[] payload, string runTemplateId, string to)
{
	if (!_validateAddress(to))
		return null;

	var adhocForward = new
	{
		MobileNumber = to,
		ExpiresInSeconds = 1800, // Default 30min
		RunTemplateId = runTemplateId,
		CompressedProcessedData = _compressJson(payload)
	};

	var request = new RestRequest("api/adhoc/forwardsms", Method.POST);
	request.AddParameter("Application/Json", JsonConvert.SerializeObject(adhocForward), ParameterType.RequestBody);
	var adHocResponse = await _restClient.ExecutePostAsync<AdhocViewResponseModel>(request);

	if (adHocResponse.StatusCode != System.Net.HttpStatusCode.OK)
		throw new InvalidOperationException($"Failed to post the adhoc data: {adHocResponse.StatusDescription}\n{adHocResponse.Content}");

	return adHocResponse.Data;
}

private bool _validateAddress(string to)
{
	if (string.IsNullOrEmpty(to))
	{
		"Skip sending because to address is empty".Dump();
		return false;
	}
	return true;
}

private void _login(string baseUri, string username, string password)
{
	_restClient = new RestClient(baseUri);

	if (_restClient == null)
		throw new InvalidOperationException("Failed to open a connection the api");

	var loggedInResponse = _loginRestResponse(_restClient, username, password);
	_restClient.AddDefaultHeader("Authorization", string.Format("{0} {1}", loggedInResponse.Data.token_type, loggedInResponse.Data.access_token));
}

private byte[] _compressJson(byte[] normalBytes)
{
	using (Stream memOutput = new MemoryStream())

	using (var zipOutput = new GZipOutputStream(memOutput))
	{
		zipOutput.SetLevel(9);

		zipOutput.Write(normalBytes, 0, normalBytes.Length);
		zipOutput.Finish();

		var newBytes = new byte[memOutput.Length];
		memOutput.Seek(0, SeekOrigin.Begin);
		memOutput.Read(newBytes, 0, newBytes.Length);

		zipOutput.Close();

		return newBytes;
	}
}

private IRestResponse<LoggedInTokenModel> _loginRestResponse(IRestClient client, string username, string password)
{
	const string baseUri = "oauth";
	var request = new RestRequest(baseUri, Method.POST);
	request.AddParameter("username", username);
	request.AddParameter("password", password);
	request.AddParameter("grant_type", "password");
	request.AddParameter("client_id", "ifscloudapp");
	// action
	var restResponse = client.Execute<LoggedInTokenModel>(request);
	return restResponse;
}

public class LoggedInTokenModel
{
	public string access_token { get; set; }
	public string token_type { get; set; }
	public int expires_in { get; set; }
	public string userName { get; set; }
	public DateTime Issued { get; set; }
	public DateTime Expires { get; set; }
}

public class AdhocViewResponseModel
{
	public bool IsSuccess { get; set; }
	public string AdhocId { get; set; }
	public string ViewerUrl { get; set; }
}