<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Collections.Concurrent.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Threading.dll</Reference>
  <NuGetReference>FluentAssertions</NuGetReference>
  <NuGetReference>log4net</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>SharpZipLib</NuGetReference>
  <Namespace>ICSharpCode.SharpZipLib.GZip</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>RestSharp</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

private string _baseUri = "";
private string _userName = "";
private string _password = "";

private DirectoryInfo _sourceDataDirInfo = new DirectoryInfo(@"");
private string _runTemplateId = "";
private string _runDescription = $"Test API External Processor Integration - {DateTime.Now.ToString("dd-MM-yyyy")}";
private bool _runIsTrial = true;

private List<string> _trialEmailAddresses = new List<string> { "verifiedemail@yourdomain.com" };

private IRestClient _restClient;

async Task Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));
		
	_login(_baseUri, _userName, _password);

	var runProfileId = await _createRun(_runDescription, _runIsTrial, _runTemplateId, _trialEmailAddresses);

	// TODO: [OPT] IFS - Configuration Files Upload
	
	// TODO: [OPT] IFS - Static Attachment Files Upload
	
	// TODO: [OPT] IFS - Static Run Recipient Attachment Files Upload

	// Loop through all external processed json files and upload to run
	int recipientCounter = 0;
	foreach (var file in _sourceDataDirInfo.GetFiles("*.json"))
	{
		++recipientCounter;
		var runRecipientJson = File.ReadAllText(file.FullName);
		if (string.IsNullOrEmpty(runRecipientJson))
		{
			Console.WriteLine($"Recipient: {recipientCounter} has not json");
			continue;
		}
		var runRecipientInstance = _createRunRecipient(runProfileId, runRecipientJson);
		((object)runRecipientInstance).Dump("Run recipient Instance");
		if (! await _saveRunRecipient(runRecipientInstance, recipientCounter))
			throw new InvalidOperationException("Failed to save the run recipient");

	}

	await _startRun(runProfileId, _runIsTrial);
}

private async Task<string> _createRun(string runDescription,bool isTrial,string runTemplateId,List<string> trailEmailAddresses)
{
	var json = _getRunConfig(runDescription,isTrial,runTemplateId,trailEmailAddresses);
	var request = new RestRequest("api/run/createRun", Method.POST) { RequestFormat = DataFormat.Json };
	request.AddJsonBody(json);
	var response = await _restClient.ExecutePostAsync(request);
	if (response.StatusCode == HttpStatusCode.OK)
	{
		var run = _convertJson(response.Content);
		return run.Id;
	}
	throw new InvalidOperationException(string.Format("Failed to create the run: {0}-{1},\n{2}", response.StatusCode, response.StatusDescription, response.ErrorMessage));
}

private async Task<bool> _saveRunRecipient(dynamic jsonAsInstance, int counter)
{
	var request = new RestRequest("api/runRecipient/createRunRecipient", Method.POST);
	request.AddParameter("Application/Json", JsonConvert.SerializeObject(jsonAsInstance), ParameterType.RequestBody);
	var response = await _restClient.ExecutePostAsync(request);
	if (response.StatusCode == HttpStatusCode.OK)
	{
		var recipient = _convertJson(response.Content);
		if (recipient == null)
			throw new InvalidCastException("Failed to cast string as instance");

		Console.WriteLine($"Successfully added recipient {counter} with Id = {recipient.Id}");
		return true;
	}
	Console.WriteLine($"Error adding new recipient {counter}.\n\t{response.StatusCode}-{response.StatusDescription}:\n\t{response.ErrorMessage}");
	return false;
}

private dynamic _createRunRecipient(string runId, string json)
{
	var jsonAsInstance = _convertJsonToDataFormat(json);
	if (jsonAsInstance == null)
		throw new InvalidOperationException("Failed to parse the JSON:\n\t" + json);

	return new
	{
		RunId = runId,
		ExternalId = jsonAsInstance.Id, //Unique Number - Account Number etc.
		Name = jsonAsInstance.Name,
		Email = string.IsNullOrEmpty(jsonAsInstance.Email) ? "#" : jsonAsInstance.Email,   //This will be the Email of the client if this is valid else # - dont leave blank
		Mobile = jsonAsInstance.Mobile, //International Format
		PossibleOutputChannels = new Dictionary<int, int[]>(){
				{0, new []{1,0}},
				{1, new []{0,1}}
			},
		IfsDataLabel = "Sample document",
		CompressedIfsData = _compressJson(json)
	};
}

private byte[] _compressJson(string json)
{
	var normalBytes = _getBytes(json);
	using (Stream memOutput = new MemoryStream())

	using (var zipOutput = new GZipOutputStream(memOutput))
	{
		zipOutput.SetLevel(9);

		zipOutput.Write(normalBytes, 0, normalBytes.Length);
		zipOutput.Finish();

		var newBytes = new byte[memOutput.Length];
		memOutput.Seek(0, SeekOrigin.Begin);
		memOutput.Read(newBytes, 0, newBytes.Length);

		zipOutput.Close();

		return newBytes;
	}
}

private byte[] _getBytes(string str)
{
	var bytes = new byte[str.Length * sizeof(char)];
	Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
	return bytes;
}

private async Task _startRun(string runId, bool isTrial)
{
	var request = new RestRequest($"api/run/{runId}/startRun/{isTrial}/0", Method.POST);
	var response = await _restClient.ExecutePostAsync(request);

	if (response.StatusCode == HttpStatusCode.OK)
	{
		var run = _convertJson(response.Content);
		return;
	}
	throw new InvalidOperationException(string.Format("Failed to create the run: {0}-{1},\n{2}", response.StatusCode, response.StatusDescription, response.ErrorMessage));
}

private dynamic _getRunConfig(string runDescription, bool isTrial, string runTemplateId,List<string> trialEmailAddresses)
{
	return new
	{
	 	// Name of the run that will be displayd to users
		Name = runDescription,
		
		// Will samples be reviewed before release 	  
		IsTrial = isTrial, 
		
		// Default Value
		IsPukLess = true, 
		
		// For future dated runs. This does require the run to be in non-trial state
		//ScheduleDateTime = DateTime.Now.AddMonths(1),
		
		// Template id to be used when creating the run
		RunTemplateId = runTemplateId,
		
		// Should Trial be true, then what should setup be
		TrialOptions = new
		{
			// Should a trial be sent immediatly when the all data is uploaded
			IsAutoTrialEnabled = false,
			
			// Default number of recipient selected for trial -- Try never to exceed 10, it will overwelm users
			/*
				OneRandomInfoslip,FiveRandomInfoslip,TenRandomInfoslip,ThirtyRandomInfoSlips,SpecificInfoslip,AllInfoslip
			*/
			RecipientSelection = 1,
			
			/*
			 UserKey,SampleKey,NoKey
			*/
			UserKeyOption = 2,
			
			/*
			All,Ifs,Pdf,Sms
			*/
			FilterOutputType = 0,
			
			SendToUser = trialEmailAddresses,
			
			// Leave as uninitialized list, will be loaded by {RecipientSelection} specified
			RecipientList = new List<string>()
		}
	};
}

private InputDataFormat _convertJsonToDataFormat(string json)
{
	return JsonConvert.DeserializeObject<InputDataFormat>(json);
}

private dynamic _convertJson(string json)
{
	return JsonConvert.DeserializeObject<dynamic>(json);
}

private void _login(string baseUri, string username, string password)
{
	_restClient = new RestClient(baseUri);

	if (_restClient == null)
		throw new InvalidOperationException("Failed to open a connection the api");

	var loggedInResponse = _loginRestResponse(_restClient, username, password);
	_restClient.AddDefaultHeader("Authorization", string.Format("{0} {1}", loggedInResponse.Data.token_type, loggedInResponse.Data.access_token));
}

private IRestResponse<LoggedInTokenModel> _loginRestResponse(IRestClient client, string username, string password)
{
  const string baseUri = "oauth";
  var request = new RestRequest(baseUri, Method.POST);
  request.AddParameter("username", username);
  request.AddParameter("password", password);
  request.AddParameter("grant_type", "password");
  request.AddParameter("client_id", "ifscloudapp");
  // action
  var restResponse = client.Execute<LoggedInTokenModel>(request);
  return restResponse;
}

public class LoggedInTokenModel
{
	public string access_token { get; set; }
	public string token_type { get; set; }
	public int expires_in { get; set; }
	public string userName { get; set; }
	public DateTime Issued { get; set; }
	public DateTime Expires { get; set; }
}

public class InputDataFormat
{
	public string Id {get;set;}
	public string Name {get;set;}
	public string Email {get;set;}
	public string Mobile {get;set;}
}