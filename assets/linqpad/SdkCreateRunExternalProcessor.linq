<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Collections.Concurrent.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Threading.dll</Reference>
  <NuGetReference>IIAB.Sdk.Samples</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>IIAB.Sdk</Namespace>
  <Namespace>IIAB.Web.Models</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

private string _baseUri = "";
private string _userName = "";
private string _password = "";

private DirectoryInfo _sourceDataDirInfo = new DirectoryInfo(@"");
private string _runTemplateId = "";
private string _runDescription = $"Test API External Processor Integration - {DateTime.Now.ToString("dd-MM-yyyy")}";
private bool _runIsTrial = true;

private List<string> _trialEmailAddresses = new List<string> { "verifiedemail@yourdomain.com" };

private InfoSlipsApiSession _session;

async Task Main()
{
	if (string.IsNullOrEmpty(_password))
		_password = Util.ReadLine(string.Format("Please enter the password for [{0}]", _userName));

	_session = new InfoSlipsApiSession(new System.Net.Http.HttpClient { BaseAddress = new Uri(_baseUri) });
	await _session.Authorize.Login(_userName, _password);

	var run = await _session.Runs.Create(new CreateRunModel{
			// Name of the run that will be displayd to users
			Name = _runDescription,

			// Will samples be reviewed before release 	  
			IsTrial = _runIsTrial,

			// Default Value
			IsPukLess = true,

			// For future dated runs. This does require the run to be in non-trial state
			//ScheduleDateTime = DateTime.Now.AddMonths(1),

			// Template id to be used when creating the run
			RunTemplateId = _runTemplateId,

			// Should Trial be true, then what should setup be
			TrialOptions = new TrialOptionsModel
			{
				// Should a trial be sent immediatly when the all data is uploaded
				IsAutoTrialEnabled = false,

				// Default number of recipient selected for trial -- Try never to exceed 10, it will overwelm users
				/*
					OneRandomInfoslip,FiveRandomInfoslip,TenRandomInfoslip,ThirtyRandomInfoSlips,SpecificInfoslip,AllInfoslip
				*/
				RecipientSelection = IIAB.Core.Common.Enum.RecipientSelectionOptions.OneRandomInfoslip,

				/*
				 UserKey,SampleKey,NoKey
				*/
				UserKeyOption = IIAB.Core.Common.Enum.UserKeyOptions.NoKey,

				/*
				All,Ifs,Pdf,Sms
				*/
				FilterOutputType = IIAB.Core.Common.Enum.FilterOutputTypeCodes.All,

				SendToUser = _trialEmailAddresses,

				// Leave as uninitialized list, will be loaded by {RecipientSelection} specified
				RecipientList = new List<string>()
			}
		});
	

	// TODO: [OPT] IFS - Configuration Files Upload
	
	// TODO: [OPT] IFS - Static Attachment Files Upload
	
	// TODO: [OPT] IFS - Static Run Recipient Attachment Files Upload

	// Loop through all external processed json files and upload to run
	int recipientCounter = 0;
	foreach (var file in _sourceDataDirInfo.GetFiles("*.json"))
	{
		++recipientCounter;
		var json = File.ReadAllText(file.FullName);
		var jsonAsInstance = JsonConvert.DeserializeObject<InputDataFormat>(json);
				
		if (jsonAsInstance == null)
		{
			Console.WriteLine($"Recipient: {recipientCounter} has no json data");
			continue;
		}
		
		var runRecipientModel = await _session.RunRecipients.Create(new CreateRunRecipientModel
		{
			RunId = run.Id,
			ExternalId = jsonAsInstance.Id, //Unique Number - Account Number etc.
			Name = jsonAsInstance.Name,
			Email = string.IsNullOrEmpty(jsonAsInstance.Email) ? "#" : jsonAsInstance.Email,   //This will be the Email of the client if this is valid else # - dont leave blank
			Mobile = jsonAsInstance.Mobile, //International Format
			IfsDataLabel = $"{jsonAsInstance.Id}_0_Sample",
			GroupName = "Statement",
			IfsData = JsonConvert.DeserializeObject<dynamic>(json)
		});
					
		runRecipientModel.Dump("Run recipient Instance");

	}

	await _session.Runs.StartRun(run.Id,_runIsTrial,0);
}

public class InputDataFormat
{
	public string Id {get;set;}
	public string Name {get;set;}
	public string Email {get;set;}
	public string Mobile {get;set;}
}